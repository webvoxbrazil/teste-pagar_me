<?php

namespace PagarmeCoreApiLib;

require_once  __DIR__ . "/vendor/autoload.php";
require __DIR__ . "/def.php";

$basicAuthUserName = API_PAGARME; // O nome de usuário a ser usado com autenticação básica 
$basicAuthPassword = SENHA_PAGARME; // A senha para usar com autenticação básica

$client = new PagarmeCoreApiClient($basicAuthUserName, $basicAuthPassword);
$body = new Models\CreateOrderRequest([
   "items" => [
      "amount" => 3000,
      "description" => "Chaveiro do Teressact",
      "quantity" => 1
   ],
   "customer" => [
      "name" => "Tony Stark"
   ],
   "payments" => [
      "amount" => 3000,
      "payment_method" => "checkout",
      "checkout" => [
         "expires_in" => 120,
         "billing_address_editable" => false,
         "customer_editable" => true,
         "accepted_payment_methods" => [
            "credit_card",
            "boleto"
         ],
         "success_url" => "https://www.pagar.me",
         "boleto" => [
            "bank" => "033",
            "instructions" => "Pagar até o vencimento",
            "due_at" => "2020-07-25T00:00:00Z"
         ],
         "credit_card" => [
            "installments" => [
               [
                  "number" => 1,
                  "total" => 3000
               ],
               [
                  "number" => 2,
                  "total" => 3000
               ]
            ]
         ]
      ]
   ],
   "code" => "cgzuz9kh",
   "customerId" => "cgzuz9kh",
   "metadata" => [],
   "closed" => True,
]);
var_dump($body);
$orders = $client->getOrders();
// var_dump($orders);
$result = $orders->createOrder($body);
var_dump($result);
